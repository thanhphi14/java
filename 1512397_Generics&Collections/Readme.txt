Bài tập: Generic & Collection

*THÔNG TIN SINH VIÊN
Họ tên: Ngô Thanh Phi
MSSV: 	1512397
Email: 	thanhphi.dd@gmail.com

*HƯỚNG DẪN BIÊN DỊCH VÀ CHẠY MÃ NGUỒN
B1: Copy file "jcalendar-1.4.jar" tại thư mục Libs vào thư mục Source
    Copy thư mục "resources" trong thư mục Release vào thư mục Source
B2: Mở Command window tới thư mục Source chứa các file mã nguồn
B3: Gõ lệnh sau vào Command window để biên dịch chương trình
	javac -encoding utf-8 -cp .;jcalendar-1.4.jar G_CGUI.java
B4: Gõ lệnh sau vào Command window để thực thi chương trình
	java -cp .;jcalendar-1.4.jar G_CGUI

** Lưu ý:
	Thư mục "resources" phải có ít nhất 2 file sau để có danh sách từ điển
		- Anh_Viet.xml
		- Viet_Anh.xml
	Ngoài ra còn có các file hỗ trợ lưu các dữ liệu cần thiết
		- File: FavoriteEn-Vi.xml & FavoriteVi-En.xml
		- File: HistoryEn.xml & HistoryVi.xml
		
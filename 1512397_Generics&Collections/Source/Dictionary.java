/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.*;
/**
 *
 * @author NgoThanhPhi
 */
public class Dictionary 
{
    public TreeMap loadDictionary(String fileName)
    {
        TreeMap tree = new TreeMap();
        try {
            File xmlFile = new File(fileName);            
            if(xmlFile.exists())
            {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlFile);

                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("record");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        String word = eElement.getElementsByTagName("word").item(0).getTextContent();
                        String meaning = eElement.getElementsByTagName("meaning").item(0).getTextContent();
                        tree.put(word, meaning);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tree;
    }
    
    public void saveFavoriteList(Vector<String> list, String fileName)
    {
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            // root Element
            Element rootElement = doc.createElement("favorites");
            doc.appendChild(rootElement);
            
            //word Element
            for(int i=0; i< list.size();i++){
                Element word = doc.createElement("word");
                word.appendChild(doc.createTextNode(list.get(i)));
                rootElement.appendChild(word);
            }
            
            // write the content into xml file
            File xmlfile = new File(fileName);
            if(!xmlfile.exists())
                xmlfile.createNewFile();
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(xmlfile);
            transformer.transform(source, result);
        } catch(Exception e) {
            e.printStackTrace();
        }        
    }
    
    public Vector<String> loadFavoriteList(String fileName)
    {
        Vector<String> list = new Vector<String>();
        try {
            File xmlfile = new File(fileName);
            if(xmlfile.exists())
            {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlfile);

                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("word");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE){
                        String word = nNode.getTextContent();
                        list.add(word);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public void saveHistoryLookup(Vector<HistoryLookup> list, String fileName)
    {
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            // root Element
            Element history = doc.createElement("history");
            doc.appendChild(history);
            
            for(int i=0; i< list.size();i++)
            {   
                // wordlist Element
                Element wordlist = doc.createElement("wordlists");
                history.appendChild(wordlist);                
                // setting attribute to element
                Attr attr = doc.createAttribute("date");
                attr.setValue(list.get(i).getDate());
                wordlist.setAttributeNode(attr);
                
                Set<String> keys = list.get(i).getKey();
                for(String key : keys){
                    Element word = doc.createElement("word");
                    Attr attrType = doc.createAttribute("freq");
                    attrType.setValue(list.get(i).getValue(key));
                    word.setAttributeNode(attrType);
                    word.appendChild(doc.createTextNode(key));
                    wordlist.appendChild(word);
                }
            }
            
            // write the content into xml file
            File xmlfile = new File(fileName);
            if(!xmlfile.exists())
                xmlfile.createNewFile();
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(xmlfile);
            transformer.transform(source, result);
        } catch(Exception e) {
            e.printStackTrace();
        }        
    }
    
    public Vector<HistoryLookup> loadHistoryLookup(String fileName)
    {
        Vector<HistoryLookup> vHistory = new Vector<HistoryLookup>();
        try {
            File xmlFile = new File(fileName);            
            if(xmlFile.exists())
            {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlFile);

                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("wordlists");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    HistoryLookup hLookup = new HistoryLookup();
                    String date = "";
                    TreeMap tree = null;
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        tree = new TreeMap();                                                
                        date = eElement.getAttribute("date");
                        
                        NodeList nListWord = eElement.getElementsByTagName("word");
                        for(int i=0; i< nListWord.getLength();i++){
                            Node nNode2 = nListWord.item(i);
                            if(nNode2.getNodeType() == Node.ELEMENT_NODE){
                                Element eElement2 = (Element) nNode2;
                                String freq = eElement2.getAttribute("freq");
                                String word = nNode2.getTextContent();                                
                                tree.put(word, Integer.parseInt(freq));
                            }
                        }                        
                    }
                    hLookup.putData(date, tree);
                    vHistory.add(hLookup);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vHistory;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author NgoThanhPhi
 */
public class HistoryLookup {
    private String dateSearch;
    private TreeMap treeHistory;
    
    HistoryLookup(){
        this.dateSearch = null;
        this.treeHistory = new TreeMap();
    }
    
    public void putFirstData(String date, String word)
    {
        this.dateSearch = date;
        this.treeHistory.put(word, 1);
    }
    
    public void putData(String date, TreeMap tree)
    {
        this.dateSearch = date;
        this.treeHistory = tree;
    }
    
    public String getDate(){
        return this.dateSearch;
    }
    
    public TreeMap getTree(){
        return this.treeHistory;
    }
    
    public Set<String> getKey(){
        return this.treeHistory.keySet();
    }
    
    public String getValue(String key){
        return this.treeHistory.get(key).toString();
    }
    
    public void changeValue(String key, int newValue){
        this.treeHistory.replace(key, newValue);
    }
    
    public void putKeyValue(String key){
        this.treeHistory.put(key, 1);
    }
}

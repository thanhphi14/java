/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import org.hibernate.*;
import java.util.*;
import entities.*;
import javax.swing.JOptionPane;

/**
 *
 * @author NgoThanhPhi
 */
public class DiemDanhDAO {

    private final SessionFactory sf = HibernateUtil.getSessionFactory();

    public List<Diemdanh> findAll() {
        try {
            sf.getCurrentSession().beginTransaction();
            List<Diemdanh> list = sf.getCurrentSession().createCriteria(Diemdanh.class).list();
            sf.getCurrentSession().close();
            return list;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public Diemdanh find(DiemdanhId ddId) {
        try {
            sf.getCurrentSession().beginTransaction();
            Diemdanh dd = (Diemdanh) sf.getCurrentSession().get(Diemdanh.class, ddId);
            sf.getCurrentSession().close();
            return dd;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public boolean delete(Diemdanh dd) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(dd);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }

    public boolean save(Diemdanh dd) {
        Diemdanh temp = find(dd.getId());
        if (temp != null) {
            JOptionPane.showMessageDialog(null, "Đã tồn tại môn học " + temp.getId().getMaMh() + " và sinh viên " + temp.getId().getMaSv() + " !");
            return false;
        } else {
            try {
                sf.getCurrentSession().beginTransaction();
                sf.getCurrentSession().save(dd);
                sf.getCurrentSession().getTransaction().commit();
                sf.getCurrentSession().close();
                return true;
            } catch (HibernateException e) {
                sf.getCurrentSession().getTransaction().rollback();
                sf.getCurrentSession().close();
                return false;
            }
        }
    }

    public boolean update(Diemdanh dd) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().update(dd);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import org.hibernate.*;
import entities.*;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author NgoThanhPhi
 */
public class MonHocDAO {

    private final SessionFactory sf = HibernateUtil.getSessionFactory();

    public List<Monhoc> findAll() {
        try {
            sf.getCurrentSession().beginTransaction();
            List<Monhoc> list = sf.getCurrentSession().createCriteria(Monhoc.class).list();
            sf.getCurrentSession().close();
            return list;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public Monhoc find(String username) {
        try {
            sf.getCurrentSession().beginTransaction();
            Monhoc mh = (Monhoc) sf.getCurrentSession().get(Monhoc.class, username);
            sf.getCurrentSession().close();
            return mh;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public boolean delete(Monhoc mh) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(mh);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }

    public boolean save(Monhoc mh) {
        Monhoc temp = find(mh.getMaMh());
        if (temp != null) {
            JOptionPane.showMessageDialog(null, "Đã tồn tại môn học " + mh.getMaMh() + " !");
            return false;
        } else {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().save(mh);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }}

    public boolean update(Monhoc mh) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().saveOrUpdate(mh);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import org.hibernate.*;
import java.util.*;
import entities.*;
import javax.swing.JOptionPane;

/**
 *
 * @author NgoThanhPhi
 */
public class SinhVienDAO {

    private final SessionFactory sf = HibernateUtil.getSessionFactory();

    public List<Sinhvien> findAll() {
        try {
            sf.getCurrentSession().beginTransaction();
            List<Sinhvien> list = sf.getCurrentSession().createCriteria(Sinhvien.class).list();
            sf.getCurrentSession().close();
            return list;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public Sinhvien find(String mssv) {
        try {
            sf.getCurrentSession().beginTransaction();
            Sinhvien sv = (Sinhvien) sf.getCurrentSession().get(Sinhvien.class, mssv);
            sf.getCurrentSession().close();
            return sv;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public boolean delete(Sinhvien sv) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(sv);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }

    public boolean save(Sinhvien sv) {
        Sinhvien temp = find(sv.getMaSv());
        if (temp != null) {
            return false;
        } else {
            try {
                sf.getCurrentSession().beginTransaction();
                sf.getCurrentSession().save(sv);
                sf.getCurrentSession().getTransaction().commit();
                sf.getCurrentSession().close();
                return true;
            } catch (HibernateException e) {
                sf.getCurrentSession().getTransaction().rollback();
                sf.getCurrentSession().close();
                return false;
            }
        }
    }

    public boolean update(Sinhvien sv) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().update(sv);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }
}

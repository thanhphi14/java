/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import org.hibernate.*;
import java.util.*;
import entities.*;
import javax.swing.JOptionPane;

/**
 *
 * @author NgoThanhPhi
 */
public class TaiKhoanDAO {

    private final SessionFactory sf = HibernateUtil.getSessionFactory();

    public List<Taikhoan> findAll() {
        try {
            sf.getCurrentSession().beginTransaction();
            List<Taikhoan> list = sf.getCurrentSession().createCriteria(Taikhoan.class).list();
            sf.getCurrentSession().close();
            return list;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public Taikhoan find(String username) {
        try {
            sf.getCurrentSession().beginTransaction();
            Taikhoan tk = (Taikhoan) sf.getCurrentSession().get(Taikhoan.class, username);
            sf.getCurrentSession().close();
            return tk;
        } catch (HibernateException e) {
            sf.getCurrentSession().close();
            return null;
        }
    }

    public boolean delete(Taikhoan tk) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(tk);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }

    public boolean save(Taikhoan tk) {
        Taikhoan temp = find(tk.getTaiKhoan());
        if (temp != null) {
            JOptionPane.showMessageDialog(null, "Đã tồn tại tài khoản " + tk.getTaiKhoan() + " !");
            return false;
        } else {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().save(tk);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }}

    public boolean update(Taikhoan tk) {
        try {
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().saveOrUpdate(tk);
            sf.getCurrentSession().getTransaction().commit();
            sf.getCurrentSession().close();
            return true;
        } catch (HibernateException e) {
            sf.getCurrentSession().getTransaction().rollback();
            sf.getCurrentSession().close();
            return false;
        }
    }
}

package entities;
// Generated Apr 15, 2018 10:09:11 AM by Hibernate Tools 4.3.1



/**
 * Taikhoan generated by hbm2java
 */
public class Taikhoan  implements java.io.Serializable {


     private String taiKhoan;
     private String matKhau;
     private boolean vaiTro;

    public Taikhoan() {
    }

	
    public Taikhoan(String taiKhoan, boolean vaiTro) {
        this.taiKhoan = taiKhoan;
        this.vaiTro = vaiTro;
    }
    public Taikhoan(String taiKhoan, String matKhau, boolean vaiTro) {
       this.taiKhoan = taiKhoan;
       this.matKhau = matKhau;
       this.vaiTro = vaiTro;
    }
   
    public String getTaiKhoan() {
        return this.taiKhoan;
    }
    
    public void setTaiKhoan(String taiKhoan) {
        this.taiKhoan = taiKhoan;
    }
    public String getMatKhau() {
        return this.matKhau;
    }
    
    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }
    public boolean isVaiTro() {
        return this.vaiTro;
    }
    
    public void setVaiTro(boolean vaiTro) {
        this.vaiTro = vaiTro;
    }




}



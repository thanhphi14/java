/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;
/**
 *
 * @author NgoThanhPhi
 */
public class Bai1 {
    /**
     * @param args the command line arguments
     */
    private static final String DELIMITER = ",";
    private static final String NEW_LINE = "\n";
    private static final String QUOTE = "\"";
    private static final String FILE_HEADER = "MHS,TenHS,Diem,Image,Diachi,Ghichu";
        
    public static void main(String[] args) {
        // TODO code application logic here 
        Scanner sc = new Scanner(System.in);
        String fileName = "StudentList.csv";
        SList sList = new SList();        
        String Mhs;
        String TenHs;
        double diem;
        String image;
        String diachi;
        String ghichu;
        sList.UpdateSList(readCSVFile(fileName));
        int num;
        do{
            System.out.println("==============QUAN LY HOC SINH==============");
            System.out.println("1. Them hoc sinh.");
            System.out.println("2. Cap nhat thong tin hoc sinh.");
            System.out.println("3. Xoa hoc sinh.");
            System.out.println("4. Xem danh sach hoc sinh.");
            System.out.println("5. Thoat.\n");
            System.out.print("Lua chon: ");
            String str_num = sc.nextLine();
            num = Integer.parseInt(str_num);
            switch(num){
                case 1:
                    System.out.println("----------Them hoc sinh----------");
                    System.out.println("Nhap thong tin hoc sinh moi");
                    System.out.print("Nhap ma so hoc sinh: ");
                    Mhs = sc.nextLine();
                    System.out.print("Nhap ten hoc sinh: ");
                    TenHs = sc.nextLine();
                    do{
                        String point;
                        System.out.print("Nhap diem: ");
                        point = sc.nextLine();
                        diem = Double.parseDouble(point);
                        if (diem < 0 || diem > 10)
                            System.out.println("Diem khong hop le!"); 
                    }while(diem < 0 || diem > 10); 
                    System.out.print("Nhap duong dan hinh: ");
                    image = sc.nextLine();
                    System.out.print("Nhap dia chi: ");
                    diachi = sc.nextLine();
                    System.out.print("Nhap ghi chu: ");
                    ghichu = sc.nextLine();
                    System.out.print("Da them 1 hoc sinh\n\n");
                    sList.InsertStudent(new Student(Mhs, TenHs, diem, image, diachi, ghichu));
                    break;
                case 2:
                    if(sList.GetList().size() == 0)
                        System.out.println("Chua co hoc sinh nao.\n");
                    else{
                        System.out.println("----------Cap nhat thong tin hoc sinh----------");
                        List<Student> tempList = new ArrayList<Student>();
                        tempList = sList.GetList();
                        Boolean flagEqual = false;
                        int ErrCount = 0;
                        while(flagEqual == false){
                            System.out.print("Nhap ma hoc sinh: ");
                            Mhs = sc.nextLine();
                            for(Student std : tempList){
                                if(std.GetMHS().equals(Mhs)){
                                    flagEqual = true;
                                    int choice;
                                    do{
                                        System.out.println(".......Chon thong tin chinh sua.......");
                                        System.out.println("1. Ma hoc sinh");
                                        System.out.println("2. Ten hoc sinh");
                                        System.out.println("3. Diem");
                                        System.out.println("4. Duong dan hinh");
                                        System.out.println("5. Dia chi");
                                        System.out.println("6. Ghi chu");
                                        System.out.println("7. Quay lai\n");
                                        System.out.print("Lua chon: ");
                                        String str_choice = sc.nextLine();
                                        choice = Integer.parseInt(str_choice);

                                        switch(choice){
                                            case 1:
                                                System.out.print("Nhap Ma so moi: ");
                                                String MHs = sc.nextLine();
                                                std.ChangeMHS(MHs);
                                                System.out.println("Da chinh sua ma hoc sinh.\n");
                                                break;
                                            case 2:
                                                System.out.print("Nhap Ten moi: ");
                                                String TenHS = sc.nextLine();
                                                std.ChangeTenHS(TenHS);
                                                System.out.println("Da chinh sua ten hoc sinh.\n");
                                                break;    
                                            case 3:
                                                System.out.print("Nhap Diem moi: ");
                                                String Diem = sc.nextLine();
                                                std.ChangeDiem(Double.parseDouble(Diem)); 
                                                System.out.println("Da chinh sua Diem.\n");
                                                break;
                                            case 4:
                                                System.out.print("Nhap Duong dan hinh moi: ");
                                                String Image = sc.nextLine();
                                                std.ChangeImage(Image);
                                                System.out.println("Da chinh sua duong dan hinh.\n");
                                                break;
                                            case 5:
                                                System.out.print("Nhap Dia chi moi: ");
                                                String Diachi = sc.nextLine();
                                                std.ChangeDiachi(Diachi);
                                                System.out.println("Da chinh sua dia chi.\n");
                                                break;  
                                            case 6:
                                                System.out.print("Nhap Ghi chu moi: ");
                                                String Ghichu = sc.nextLine();
                                                std.ChangeGhichu(Ghichu);
                                                System.out.println("Da chinh sua ghi chu.\n");
                                                break;  
                                        }
                                    }while (choice != 7);                            
                                }
                            }
                            if(flagEqual == false){
                                System.out.println("Khong co hoc sinh voi ma so " + Mhs);
                                ErrCount++;
                            }

                            if(ErrCount == 3){
                                System.out.println("Ban da nhap sai 3 lan, moi quay lai menu !!!\n");
                                break;
                            }
                        }
                    }
                    break;
                case 3:
                    if(sList.GetList().size() == 0)
                        System.out.println("Chua co hoc sinh nao.\n");
                    else{
                        System.out.println("----------Xoa hoc sinh----------");

                        List<Student> tempList2 = new ArrayList<Student>();
                        tempList2 = sList.GetList();
                        Boolean flagEqual2 = false;
                        int ErrCount2 = 0;
                        while(flagEqual2 == false){
                            System.out.print("Nhap ma hoc sinh: ");
                            Mhs = sc.nextLine();
                            for(int i=0; i<tempList2.size(); i++){
                                if(tempList2.get(i).GetMHS().equals(Mhs)){
                                    flagEqual2 = true;
                                    tempList2.remove(tempList2.get(i));
                                    sList.UpdateSList(tempList2);
                                    System.out.println("Da xoa hoc sinh voi ma so " + Mhs + "\n");
                                    break;
                                }                        
                            }
                            if(flagEqual2 == false){
                                System.out.println("Khong co hoc sinh voi ma so " + Mhs);
                                ErrCount2++;
                            }

                            if(ErrCount2 == 3){
                                System.out.println("Ban da nhap sai 3 lan, moi quay lai menu !!!\n");
                                break;
                            }
                        }
                    }
                    break;
                case 4:
                    if(sList.GetList().size()==0)
                        System.out.println("Chua co hoc sinh nao.\n");
                    else{
                        int choose;
                        do{
                            System.out.println("----------Xem danh sach hoc sinh----------");
                            System.out.println("1. Ma hoc sinh tang dan");
                            System.out.println("2. Ma hoc sinh giam dan");
                            System.out.println("3. Diem tang dan");
                            System.out.println("4. Diem giam dan");
                            System.out.println("5. Quay lai\n");
                            System.out.print("Lua chon: ");
                            String str_choose = sc.nextLine();
                            choose = Integer.parseInt(str_choose);
                            switch(choose){
                                case 1: 
                                    System.out.println(".......Danh sach hoc sinh tang dan theo ma so.......");
                                    sList.MHSAscending();
                                    sList.WatchList();
                                    break;
                                case 2:
                                    System.out.println(".......Danh sach hoc sinh giam dan theo ma so.......");
                                    sList.MHSDescending();
                                    sList.WatchList();
                                    break;
                                case 3:
                                    System.out.println(".......Danh sach hoc sinh tang dan theo diem.......");
                                    sList.DiemAscending();
                                    sList.WatchList();
                                    break;
                                case 4:
                                    System.out.println(".......Danh sach hoc sinh giam dan theo diem.......");
                                    sList.DiemDescending();
                                    sList.WatchList();
                                    break;
                            }
                        }while(choose != 5);
                    }
                    break;
            }                       
        }while(num != 5);
        
        sc.close();        
        List<Student> writeList = new ArrayList<Student>();
        writeList = sList.GetList();
        writeCSVFile(fileName, writeList);
    }   
    
    public static List<Student> readCSVFile(String filename){
        File f = new File(filename);        
        List<Student> slist = new ArrayList<Student>();
        if(!f.exists()){
            System.out.println("Khong ton tai file danh sach \n=> Khong ton tai thong tin hoc sinh nao.\n");
        }
        else{
            BufferedReader br = null;
            try{
                String line;

                FileReader fr = new FileReader(filename);
                br = new BufferedReader(fr);
                line = br.readLine();
                while((line = br.readLine())!= null){
                    if(line != null){
                        String[] splitData = line.split(QUOTE);
                        String diachi = splitData[1];
                        String ghichu = splitData[3];
                        String[] splitData2 = splitData[0].split(DELIMITER);
                        String Mhs = splitData2[0];
                        String TenHs = splitData2[1];
                        double diem = Double.parseDouble(splitData2[2]);
                        String image = splitData2[3];
                        slist.add(new Student(Mhs, TenHs, diem, image, diachi, ghichu));
                    }
                }
            }catch (IOException e){
                e.printStackTrace();
            }finally{
                try{
                    if(br != null)
                        br.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }        
        }
        return slist;
    }
    
    public static void writeCSVFile(String fileName, List<Student> writeList){
        FileWriter fileWriter = null;
        
        try {
            fileWriter = new FileWriter(fileName);
            fileWriter.append(FILE_HEADER);
            fileWriter.append(NEW_LINE);
            
            for(Student std : writeList){
                fileWriter.append(std.GetMHS());
                fileWriter.append(DELIMITER);
                fileWriter.append(std.GetTenHS());
                fileWriter.append(DELIMITER);
                fileWriter.append(String.valueOf(std.GetDiem()));
                fileWriter.append(DELIMITER);
                fileWriter.append(std.GetImage());
                fileWriter.append(DELIMITER);
                fileWriter.append(QUOTE);
                fileWriter.append(std.GetDiachi());
                fileWriter.append(QUOTE);
                fileWriter.append(DELIMITER);
                fileWriter.append(QUOTE);
                fileWriter.append(std.GetGhichu());
                fileWriter.append(QUOTE);
                fileWriter.append(NEW_LINE);
                
                System.out.println("CSV file was saved successfully !!!");
            }
        } catch(IOException e){
            System.out.println("Error in CSVFileWriter !!!");
            e.printStackTrace();
        } finally{
            try{ 
                fileWriter.flush();
                fileWriter.close();
            }catch (IOException e){
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }
}

class Student{
    private String MHS;
    private String TenHS;
    private double Diem;
    private String Image;
    private String Diachi;
    private String Ghichu;
    
    public Student(){
        MHS = "";
        TenHS = "";
        Diem = 0;
        Image = "";
        Diachi= "";
        Ghichu = "";
    }     
    public Student(String MHs, String TenHs, double diem, String imagepath, String diachi, String ghichu){
        MHS = MHs;
        TenHS = TenHs;
        Diem = diem;            
        Image = imagepath;
        Diachi= diachi;
        Ghichu = ghichu;
    }
        
    public String GetMHS(){
        return MHS;
    }        
    public String GetTenHS(){
        return TenHS;
    }        
    public double GetDiem(){
        return Diem;
    }
    public String GetImage(){
        return Image;
    } 
    public String GetDiachi(){
        return Diachi;
    } 
    public String GetGhichu(){
        return Ghichu;
    } 
        
    public void ChangeMHS(String MHs){
        MHS = MHs;
    }        
    public void ChangeTenHS(String TenHs){
        TenHS = TenHs;
    }        
    public void ChangeDiem(double diem){
        Diem = diem;
    }
    public void ChangeImage(String image){
        Image = image;
    } 
    public void ChangeDiachi(String diachi){
        Diachi = diachi;
    } 
    public void ChangeGhichu(String ghichu){
        Ghichu = ghichu;
    }
}

class SList{
    private List<Student> sList;
    
    public SList(){
        sList = new ArrayList<Student>();
    }
    
    public List<Student> GetList(){
        return sList;
    }
    
    public void UpdateSList(List<Student> slist){
        sList = slist;
    }
    
    public void InsertStudent(Student std){
        sList.add(std);
    }
    
    public void MHSAscending(){
        Collections.sort(sList, new Comparator<Student>() {
        @Override
        public int compare(Student std1, Student std2) {
            return std1.GetMHS().compareTo(std2.GetMHS()); // if you want to short by name
        }});
    }
    
    public void MHSDescending(){
        Collections.sort(sList, new Comparator<Student>() {
        @Override
        public int compare(Student std1, Student std2) {
            return std2.GetMHS().compareTo(std1.GetMHS()); // if you want to short by name
        }});
    }
    
    public void DiemAscending(){
        Collections.sort(sList, new Comparator<Student>() {
        @Override
        public int compare(Student std1, Student std2) {
            if (std1.GetDiem() > std2.GetDiem()) {
                    return 1;
                } else {
                    if (std1.GetDiem() == std2.GetDiem()) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
            }
        });
    }
    
    public void DiemDescending(){
        Collections.sort(sList, new Comparator<Student>() {
        @Override
        public int compare(Student std1, Student std2) {
            if (std1.GetDiem() < std2.GetDiem()) {
                    return 1;
                } else {
                    if (std1.GetDiem() == std2.GetDiem()) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
            }
        });
    }
    
    public void WatchList(){
        for(Student std: sList){
            System.out.println("Ma hoc sinh: " + std.GetMHS());
            System.out.println("Ten hoc sinh: " + std.GetTenHS());
            System.out.println("Diem: " + std.GetDiem());
            System.out.println("Hinh: " + std.GetImage());
            System.out.println("Dia chi: " + std.GetDiachi());
            System.out.println("Ghi chu: " + std.GetGhichu());
            System.out.println();
        }
    }
}

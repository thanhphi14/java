/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.table.*;
import javax.swing.filechooser.FileSystemView;
import javax.imageio.ImageIO;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.net.URL;
/**
 *
 * @author NgoThanhPhi
 */
public class Bai2 {
    public static final String APP_TITLE = "File Manager"; //Title of the application
    private Desktop desktop; //Used to open files. 
    private FileSystemView fileSystemView; //Provides nice icons and names for files.
    private File currentFile; //currently selected File.
    private JPanel gui; //Main GUI container

    // File-system tree. Built Lazily */
    private JTree tree;
    private DefaultTreeModel treeModel;

    /** Directory listing */
    private JTable table;
    private JProgressBar progressBar;
    /** Table model for File[]. */
    private FileTableModel fileTableModel;
    private ListSelectionListener listSelectionListener;
    private boolean cellSizesSet = false;
    private int rowIconPadding = 6;

    /* File controls. */
    private JButton locateFile;
    private JButton openFile;
    private JButton renameFile;
    private JButton deleteFile;
    private JButton backFile;
    private JButton createFolder;
    private JButton createFile;
    private JButton copyFile;
    private JButton copyFolder;
    private JButton zipF;
    private JButton unzipF;
    private JButton splitFile;
    private JButton joinFile;


    /* File details. */
    private JLabel fileName;
    private JTextField path;
    private JLabel date;
    private JLabel size;
    
    private List<String> fileList;
    private List<String> fileUnlist;

    public Container getGui() {
        if (gui==null) {
            gui = new JPanel(new BorderLayout(3,3));
            gui.setBorder(new EmptyBorder(5,5,5,5));

            fileSystemView = FileSystemView.getFileSystemView();
            desktop = Desktop.getDesktop();

            JPanel detailView = new JPanel(new BorderLayout(3,3));

            table = new JTable();
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.setAutoCreateRowSorter(true);
            table.setShowVerticalLines(false);

            listSelectionListener = new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent lse) {
                    int row = table.getSelectionModel().getLeadSelectionIndex();
                    setFileDetails( ((FileTableModel)table.getModel()).getFile(row) );
                }
            };
            table.getSelectionModel().addListSelectionListener(listSelectionListener);
            JScrollPane tableScroll = new JScrollPane(table);
            Dimension d = tableScroll.getPreferredSize();
            tableScroll.setPreferredSize(new Dimension((int)d.getWidth()*2, (int)d.getHeight()));
            detailView.add(tableScroll, BorderLayout.CENTER);

            // the File tree
            DefaultMutableTreeNode root = new DefaultMutableTreeNode();
            treeModel = new DefaultTreeModel(root);

            TreeSelectionListener treeSelectionListener = new TreeSelectionListener() {
                public void valueChanged(TreeSelectionEvent tse){
                    DefaultMutableTreeNode node =
                        (DefaultMutableTreeNode)tse.getPath().getLastPathComponent();
                    showChildren(node);
                    setFileDetails((File)node.getUserObject());
                }
            };

            // show the file system roots.
            File[] roots = fileSystemView.getRoots();
            for (File fileSystemRoot : roots) {
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileSystemRoot);
                root.add( node );
                File[] files = fileSystemView.getFiles(fileSystemRoot, true);
                for (File file : files) {
                    if (file.isDirectory()) {
                        node.add(new DefaultMutableTreeNode(file));
                    }
                }
                //
            }

            tree = new JTree(treeModel);
            tree.setRootVisible(false);
            tree.addTreeSelectionListener(treeSelectionListener);
            tree.setCellRenderer(new FileTreeCellRenderer());
            tree.expandRow(0);
            JScrollPane treeScroll = new JScrollPane(tree);

            // as per trashgod tip
            tree.setVisibleRowCount(15);

            Dimension preferredSize = treeScroll.getPreferredSize();
            Dimension widePreferred = new Dimension(200, (int)preferredSize.getHeight());
            treeScroll.setPreferredSize( widePreferred );

            // details for a File
            JPanel fileMainDetails = new JPanel(new BorderLayout(4,2));
            fileMainDetails.setBorder(new EmptyBorder(0,6,0,6));

            JPanel fileDetailsLabels = new JPanel(new GridLayout(0,1,2,2));
            fileMainDetails.add(fileDetailsLabels, BorderLayout.WEST);

            JPanel fileDetailsValues = new JPanel(new GridLayout(0,1,2,2));
            fileMainDetails.add(fileDetailsValues, BorderLayout.CENTER);
            
            fileDetailsLabels.add(new JLabel("New name/Path save file/Number SubFile", JLabel.TRAILING));            
            JTextField newNameFile = new JTextField();
            fileDetailsValues.add(newNameFile);
            fileDetailsLabels.add(new JLabel("Path source/File zip/unzip", JLabel.TRAILING));            
            JTextField source = new JTextField();
            fileDetailsValues.add(source);
            fileDetailsLabels.add(new JLabel("Path dest", JLabel.TRAILING));            
            JTextField dest = new JTextField();
            fileDetailsValues.add(dest);
            fileDetailsLabels.add(new JLabel("File", JLabel.TRAILING));
            fileName = new JLabel();
            fileDetailsValues.add(fileName);
            fileDetailsLabels.add(new JLabel("Path/name", JLabel.TRAILING));
            path = new JTextField(5);
            path.setEditable(false);
            fileDetailsValues.add(path);
            fileDetailsLabels.add(new JLabel("Last Modified", JLabel.TRAILING));
            date = new JLabel();
            fileDetailsValues.add(date);
            fileDetailsLabels.add(new JLabel("File size", JLabel.TRAILING));
            size = new JLabel();
            fileDetailsValues.add(size);
            
            JToolBar toolBar = new JToolBar();
            // mnemonics stop working in a floated toolbar
            toolBar.setFloatable(false);
            
            locateFile = new JButton("Locate");
            locateFile.setMnemonic('l');
            locateFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        desktop.open(currentFile.getParentFile());
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(locateFile);

            openFile = new JButton("Open");
            openFile.setMnemonic('o');
            openFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        if(currentFile.isFile())
                            desktop.open(currentFile);
                        else{
                            File[] files = fileSystemView.getFiles(currentFile, true);
                            setTableData(files);
                        }
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(openFile);
            
            createFolder = new JButton("CreateFolder");
            createFolder.setMnemonic('C');
            createFolder.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        File theDir = new File(newNameFile.getText());
                        if(!theDir.exists()){
                            try{
                                theDir.mkdir();
                            }catch(SecurityException se){
                                se.printStackTrace();
                            }
                        }
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(createFolder);
            
            createFile = new JButton("CreateFile");
            createFile.setMnemonic('c');
            createFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        File file = new File(newNameFile.getText());                        
                        file.createNewFile();
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);                            
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(createFile);
            
            
            backFile = new JButton("Back(1)");
            backFile.setMnemonic('b');
            backFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);                            
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(backFile);
            
            renameFile = new JButton("Rename");
            renameFile.setMnemonic('r');
            renameFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        File newFileOrDirectory = new File(currentFile.getParentFile().getPath() + "/"+ newNameFile.getText());
                        currentFile.renameTo(newFileOrDirectory);
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);                            
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(renameFile);
            
            deleteFile = new JButton("Delete");
            deleteFile.setMnemonic('d');
            deleteFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        if(currentFile.isFile())
                            currentFile.delete();
                        else
                            deleteDirectory(currentFile);
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(deleteFile);
            
            copyFile = new JButton("CopyFile");
            copyFile.setMnemonic('f');
            copyFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        copyFile(source.getText(), dest.getText());
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                        source.setText("");
                        dest.setText("");
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(copyFile);
            
            copyFolder = new JButton("CopyFolder");
            copyFolder.setMnemonic('F');
            copyFolder.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        copyFolder(source.getText(), dest.getText());
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                        source.setText("");
                        dest.setText("");
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(copyFolder);
            
            zipF = new JButton("ZipFolder");
            zipF.setMnemonic('z');
            zipF.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        fileList = new ArrayList<String>();
                        generateFileList(new File(source.getText()), source.getText());
                        zipIt(source.getText());
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                        source.setText("");
                        dest.setText("");
                        newNameFile.setText("");
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(zipF);
            
            unzipF = new JButton("UnzipFolder");
            unzipF.setMnemonic('u');
            unzipF.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        fileUnlist = new ArrayList<String>();
                        unzipIt(source.getText(), newNameFile.getText());
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                        source.setText("");
                        dest.setText("");
                        newNameFile.setText("");
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(unzipF);
            
            splitFile = new JButton("SplitFile");
            splitFile.setMnemonic('s');
            splitFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        splitFile(source.getText(), Integer.parseInt(newNameFile.getText()));
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                        source.setText("");
                        dest.setText("");
                        newNameFile.setText("");
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(splitFile);
            
            joinFile = new JButton("JoinFile");
            joinFile.setMnemonic('j');
            joinFile.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    try {
                        joinFile(source.getText());
                        File[] files = fileSystemView.getFiles(currentFile.getParentFile(), true);
                        setTableData(files);
                        source.setText("");
                        dest.setText("");
                        newNameFile.setText("");
                    } catch(Throwable t) {
                        showThrowable(t);
                    }
                    gui.repaint();
                }
            });
            toolBar.add(joinFile);

            // Check the actions are supported on this platform!
            openFile.setEnabled(desktop.isSupported(Desktop.Action.OPEN));      

            JPanel fileView = new JPanel(new BorderLayout(3,3));

            fileView.add(toolBar,BorderLayout.NORTH);
            fileView.add(fileMainDetails,BorderLayout.CENTER);

            detailView.add(fileView, BorderLayout.SOUTH);

            JSplitPane splitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT,
                treeScroll,
                detailView);
            gui.add(splitPane, BorderLayout.CENTER);

            JPanel simpleOutput = new JPanel(new BorderLayout(3,3));
            progressBar = new JProgressBar();
            simpleOutput.add(progressBar, BorderLayout.EAST);
            progressBar.setVisible(false);

            gui.add(simpleOutput, BorderLayout.SOUTH);

        }
        return gui;
    }
    
    public boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(files != null){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }
    
    public boolean joinFile(String source) throws FileNotFoundException, IOException{
        String name = source.substring(source.lastIndexOf("\\") + 1);
        File file = new File(source + ".txt");
        OutputStream os = new FileOutputStream(file);
        InputStream is;
        int count = 1;
        while (true){
            String path = source + "/" + name + count + ".txt";
            File eachFile = new File(path);
            if(eachFile.exists()){
                is = new FileInputStream(eachFile);
                int i=0; 
                byte[] arr = new byte[(int)eachFile.length()];
                while((i = is.read(arr))!= -1){
                    os.write(arr, 0, i);
                }
                os.flush();
                is.close();
                count++;
            }else {
                break;
            }
        }
        os.close();
        return false;
    }
    
    public boolean splitFile(String source, int numSubFile) throws FileNotFoundException, IOException{
        File sourceFile = new File(source);
        if(sourceFile.exists() && sourceFile.isFile()){
            long sizeFile = sourceFile.length();
            long sizeSplitFile = (sizeFile / numSubFile);
            InputStream is = new FileInputStream(sourceFile);
            byte[] arr = new byte[(int)sizeSplitFile + 1];
            int k = sourceFile.getName().lastIndexOf('.');            
            String name = sourceFile.getName().substring(0, k);
            String extention = sourceFile.getName().substring(k+1);
            for (int i=1;i<=numSubFile;i++){
                int j =0;
                long a =0;
                OutputStream os = new FileOutputStream(sourceFile.getParent() + "/" + name + i + "." + extention);
                while ((j = is.read(arr))!= -1){
                    os.write(arr, 0, j);
                    a += j;
                    if(a >= sizeSplitFile){
                        break;
                    }
                }
                os.flush();
                os.close();
            }
            is.close();
            return true;
        } else {
            return false;
        }
    }
    
    public void unzipIt (String zipFile, String outputFolder){
        byte[] buffer = new byte[1254203];
        try{
            File folder = new File(outputFolder);
            if(!folder.exists()){
                folder.mkdir();
            }
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while((len = zis.read(buffer))>0){
                    fos.write(buffer, 0, len);
                }
                fos.close();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
    
    public void zipIt(String source){
        String fileZip = source + ".zip";
        //String 
        byte[] buffer = new byte[1254203];
        try{
            FileOutputStream fos = new FileOutputStream(fileZip);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String files : fileList){
                ZipEntry ze = new ZipEntry(files);
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(source + File.separator + files);
                int len;
                while((len = in.read(buffer))>0){
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            zos.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
 
    public void generateFileList(File node, String source){
        if(node.isFile()){
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString(), source));
        }
        if(node.isDirectory()){
            String[] subNode = node.list();
            for(String filename : subNode){
                generateFileList(new File(node, filename), source);
            }
        }
    }
    
    private String generateZipEntry(String file, String source){
        return file.substring(source.length() + 1, file.length());
    }
    
    public boolean copyFile(String source, String dest) throws FileNotFoundException, IOException{
        File sourceFile = new File(source);
        File destFile = new File(dest);
        sourceFile.length();
        if(sourceFile.exists()){
            FileInputStream fis = new FileInputStream(sourceFile);
            FileOutputStream fos = new FileOutputStream(destFile);
            byte[] arr = new byte[(int)sourceFile.length()];
            while((fis.read(arr))!=-1){
                fos.write(arr);
                fos.flush();
            }
            fis.close();
            fos.close();
            return true;
        } else 
            return false;
    }
    
    public boolean copyFolder(String source, String dest) throws IOException{
        File sourceFile = new File(source);
        File destFile = new File(dest);
        
        if(sourceFile.exists()){
            if(sourceFile.isFile()){
                copyFile(source, dest);                
            }
            if(sourceFile.isDirectory()){
                if(!destFile.exists()){
                    destFile.mkdirs();
                }
                File[] listFile = sourceFile.listFiles();
                for(File f : listFile){
                    copyFile(f.getAbsolutePath(), dest+"/"+f.getName());
                }
            }
            return true;
        }
        return false;
    }

    public void showRootFile() {
        // ensure the main files are displayed
        tree.setSelectionInterval(0,0);
    }
    
    private void showThrowable(Throwable t) {
        t.printStackTrace();
        JOptionPane.showMessageDialog(
            gui,
            t.toString(),
            t.getMessage(),
            JOptionPane.ERROR_MESSAGE
            );
        gui.repaint();
    }

    /** Update the table on the EDT */
    private void setTableData(final File[] files) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (fileTableModel==null) {
                    fileTableModel = new FileTableModel();
                    table.setModel(fileTableModel);
                }
                table.getSelectionModel().removeListSelectionListener(listSelectionListener);
                fileTableModel.setFiles(files);
                table.getSelectionModel().addListSelectionListener(listSelectionListener);
                if (!cellSizesSet) {
                    Icon icon = fileSystemView.getSystemIcon(files[0]);

                    // size adjustment to better account for icons
                    table.setRowHeight( icon.getIconHeight()+rowIconPadding );

                    setColumnWidth(0,-1);
                    setColumnWidth(1,150);
                    table.getColumnModel().getColumn(1).setMaxWidth(500);
                    setColumnWidth(2,80);
                    setColumnWidth(3,100);

                    cellSizesSet = true;
                }
            }
        });
    }

    private void setColumnWidth(int column, int width) {
        TableColumn tableColumn = table.getColumnModel().getColumn(column);
        if (width<0) {
            // use the preferred width of the header..
            JLabel label = new JLabel( (String)tableColumn.getHeaderValue() );
            Dimension preferred = label.getPreferredSize();
            // altered 10->14 as per camickr comment.
            width = (int)preferred.getWidth()+14;
        }
        tableColumn.setPreferredWidth(width);
        tableColumn.setMaxWidth(width);
        tableColumn.setMinWidth(width);
    }

    /** Add the files that are contained within the directory of this node.
    Thanks to Hovercraft Full Of Eels for the SwingWorker fix. */
    private void showChildren(final DefaultMutableTreeNode node) {
        tree.setEnabled(false);
        progressBar.setVisible(true);
        progressBar.setIndeterminate(true);

        SwingWorker<Void, File> worker = new SwingWorker<Void, File>() {
            @Override
            public Void doInBackground() {
                File file = (File) node.getUserObject();
                if (file.isDirectory()) {
                    File[] files = fileSystemView.getFiles(file, true); //!!
                    if (node.isLeaf()) {
                        for (File child : files) {
                            if (child.isDirectory()) {
                                publish(child);
                            }
                        }
                    }
                    setTableData(files);
                }
                return null;
            }

            @Override
            protected void process(List<File> chunks) {
                for (File child : chunks) {
                    node.add(new DefaultMutableTreeNode(child));
                }
            }

            @Override
            protected void done() {
                progressBar.setIndeterminate(false);
                progressBar.setVisible(false);
                tree.setEnabled(true);
            }
        };
        worker.execute();
    }

    /** Update the File details view with the details of this File. */
    private void setFileDetails(File file) {
        currentFile = file;
        Icon icon = fileSystemView.getSystemIcon(file);
        fileName.setIcon(icon);
        fileName.setText(fileSystemView.getSystemDisplayName(file));
        path.setText(file.getPath());        
        date.setText(new Date(file.lastModified()).toString());
        size.setText(file.length() + " bytes");
        
        JFrame f = (JFrame)gui.getTopLevelAncestor();
        if (f!=null) {
            f.setTitle(APP_TITLE + " :: " + fileSystemView.getSystemDisplayName(file) );
        }

        gui.repaint();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    // Significantly improves the look of the output in
                    // terms of the file names returned by FileSystemView!
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch(Exception weTried) {
                }
                JFrame f = new JFrame(APP_TITLE);
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                Bai2 Bai2 = new Bai2();
                f.setContentPane(Bai2.getGui());

                try {
                    URL urlBig = Bai2.getClass().getResource("fb-icon-32x32.png");
                    URL urlSmall = Bai2.getClass().getResource("fb-icon-16x16.png");
                    ArrayList<Image> images = new ArrayList<Image>();
                    images.add( ImageIO.read(urlBig) );
                    images.add( ImageIO.read(urlSmall) );
                    f.setIconImages(images);
                } catch(Exception weTried) {}

                f.pack();
                f.setLocationByPlatform(true);
                f.setMinimumSize(f.getSize());
                f.setVisible(true);

                Bai2.showRootFile();
            }
        });
    }
}

/** A TableModel to hold File[]. */
class FileTableModel extends AbstractTableModel {

    private File[] files;
    private FileSystemView fileSystemView = FileSystemView.getFileSystemView();
    private String[] columns = {"Icon", "Name", "Size", "Last Modified", "Path/name"};

    FileTableModel() {
        this(new File[0]);
    }

    FileTableModel(File[] files) {
        this.files = files;
    }

    public Object getValueAt(int row, int column) {
        File file = files[row];
        switch (column) {
            case 0:
                return fileSystemView.getSystemIcon(file);
            case 1:
                return fileSystemView.getSystemDisplayName(file);
            case 2:
                return file.length();
            case 3:
                return file.lastModified();
            case 4:
                return file.getPath();
            default:
                System.err.println("Logic Error");
        }
        return "";
    }

    public int getColumnCount() {
        return columns.length;
    }

    public Class<?> getColumnClass(int column) {
        switch (column) {
            case 0:
                return ImageIcon.class;
            case 2:
                return Long.class;
            case 3:
                return Date.class;
        }
        return String.class;
    }

    public String getColumnName(int column) {
        return columns[column];
    }

    public int getRowCount() {
        return files.length;
    }

    public File getFile(int row) {
        return files[row];
    }

    public void setFiles(File[] files) {
        this.files = files;
        fireTableDataChanged();
    }
}

/** A TreeCellRenderer for a File. */
class FileTreeCellRenderer extends DefaultTreeCellRenderer {

    private FileSystemView fileSystemView;

    private JLabel label;

    FileTreeCellRenderer() {
        label = new JLabel();
        label.setOpaque(true);
        fileSystemView = FileSystemView.getFileSystemView();
    }

    @Override
    public Component getTreeCellRendererComponent(
        JTree tree,
        Object value,
        boolean selected,
        boolean expanded,
        boolean leaf,
        int row,
        boolean hasFocus) {

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
        File file = (File)node.getUserObject();
        label.setIcon(fileSystemView.getSystemIcon(file));
        label.setText(fileSystemView.getSystemDisplayName(file));
        label.setToolTipText(file.getPath());

        if (selected) {
            label.setBackground(backgroundSelectionColor);
            label.setForeground(textSelectionColor);
        } else {
            label.setBackground(backgroundNonSelectionColor);
            label.setForeground(textNonSelectionColor);
        }

        return label;
    }
}
Tên: Ngô Thanh Phi
MSSV: 1512397

Deadline Tuần 1 - Java IO

Bài 1: Đã hoàn thành tất cả yêu cầu

Bài 2: Đã hoàn thành tất cả yêu cầu
*Lưu ý Bài 2: 
	- Chức năng: "Location, Open, Delete" chỉ cần chọn thư mục hoặc tập tin vào chọn Button là kết thúc thao tác.

	- Chức năng: "Back(1)" được thực hiện khi có 1 thư mục nào sử dụng chức năng Open trước mới thực hiện được và
		     không cần chọn thư mục hay tập tin nào. Nhưng chỉ quay lại Thư mục TRƯỚC ĐÓ.

	- Chức năng: "CreateFolder, CreateFile" phải nhập đường dẫn cùng với tên File/Folder vào ô "New name/Path save file/Number SubFile"
		     sau đó chọn vào CreateFolder/CreateFile để kết thúc thao tác.

	- Chức năng: "Rename" chỉ cần nhập tên File/Folder mới và chọn Button Rename là xong.

	- Chức năng: "CopyFile/CopyFolder" nhập đường dẫn cùng tên File/Folder nguồn vào ô "Path source/File zip/unzip" và 
		     nhập đường dẫn cùng tên File/Folder đích vào ô "Path dest", sau đó chọn Button CopyFile/CopyFolder là kết thúc.
	
	- Chức năng: "ZipFolder" nhập Đường dẫn và tên Folder muốn nén vào ô "Path source/File zip/unzip", sau đó bấm ZipFolder là xong.

	- Chức năng: "UnzipFolder" nhập đường dẫn cùng tên Folder nguồn vào ô "Path source/File zip/unzip" và 
		     nhập đường dẫn cùng tên Folder đích vào ô "New name/Path save file/Number SubFile", sau đó chọn Button "UnzipFolder" là kết thúc.

	- Chức năng: "SplitFile" nhập đường dẫn cùng tên File nguồn vào ô "Path source/File zip/unzip" và nhập số lượng File
		     muốn cắt vào ô "New name/Path save file/Number SubFile", sau đó chọn Button "SplitFile" là kết thúc.

	- Chức năng: "JoinFile" Gộp được File TXT, nhập đường dẫn và tên Thư mục chứa các File con vào ô "Path source/File zip/unzip"
		     sau đó bấm "JoinFile"  là xong.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author NgoThanhPhi
 */
public class ConnectData {

    Connection con = null;

    public boolean connectSqlServer(String port, String dataBase, String userName, String passWord) {
        String dbURL = "jdbc:sqlserver://localhost:" + port + ";databaseName=" + dataBase + ";user=" + userName + ";password=" + passWord;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(dbURL);
            if (con != null) {
                return true;
            }
        } catch (ClassNotFoundException ex) {
            System.out.print(ex.toString());
        } catch (SQLException ex) {
            System.out.print(ex.toString());
        }

        return false;
    }

    public boolean addNewStudent(Student std) {
        try {
            String strAdd = "insert into HocSinh (TenHS,NgaySinh,Ghichu,ExtInfo) values (?,?,?,?)";
            PreparedStatement psAdd = con.prepareStatement(strAdd);
            psAdd.setString(1, std.getHoTenHS());
            psAdd.setDate(2, std.getNgaySinh());
            psAdd.setString(3, std.getGhiChu());
            //nếu k chọn lại hình thì nó sẽ báo            
            byte[] img = std.getImage();
            psAdd.setBytes(4, img);
            psAdd.executeUpdate();
            psAdd.close();
            return true;
        } catch (SQLException ex) {
            System.out.print(ex.toString());
        }

        return false;
    }

    public boolean deleteStudent(int MaHS) {
        try {
            Statement stDel = con.createStatement();
            String strDel = "delete from HocSinh where MaHS = " + MaHS;

            stDel.executeUpdate(strDel);
            stDel.close();
            return true;
        } catch (SQLException ex) {
            System.out.print(ex.toString());
        }
        return false;
    }

    public boolean updateStudent(Student std) {
        try {
            String strUpdate = "update HocSinh set TenHS = ?, NgaySinh = ?, Ghichu = ?, ExtInfo = ? where MaHS = " + std.getMaHS();
            PreparedStatement psUpdate = con.prepareStatement(strUpdate);
            psUpdate.setString(1, std.getHoTenHS());
            psUpdate.setDate(2, std.getNgaySinh());
            psUpdate.setString(3, std.getGhiChu());
            //nếu k chọn lại hình thì nó sẽ báo            
            byte[] img = std.getImage();
            psUpdate.setBytes(4, img);
            psUpdate.executeUpdate();
            psUpdate.close();
            return true;
        } catch (SQLException ex) {
            System.out.print(ex.toString());
        }
        return false;
    }

    public Vector<Student> searchStudent(String searchCol, String word) {
        Vector<Student> vStudent = null;
        try {
            Statement stSelect = con.createStatement();
            String strSelect = "select * from HocSinh where " + searchCol + " like N'%" + word + "%'";
            stSelect.executeQuery(strSelect);

            vStudent = new Vector<Student>();
            ResultSet rs = stSelect.executeQuery(strSelect);
            while (rs.next()) {
                Student std = new Student();
                std.setMaHS(rs.getInt("MaHS"));
                std.setHoTenHS(rs.getNString("TenHS"));
                std.setNgaySinh(rs.getDate("NgaySinh"));
                std.setGhiChu(rs.getNString("Ghichu"));
                std.setImage(rs.getBytes("ExtInfo"));

                vStudent.add(std);
            }
            stSelect.close();
        } catch (SQLException ex) {
            System.out.print(ex.toString());
        }
        return vStudent;
    }

    public Vector<Student> selectAllStudent() {

        Vector<Student> vStudent = null;
        try {
            Statement stSelect = con.createStatement();
            String strSelect = "select * from HocSinh";
            stSelect.executeQuery(strSelect);

            vStudent = new Vector<Student>();
            ResultSet rs = stSelect.executeQuery(strSelect);
            while (rs.next()) {
                Student std = new Student();
                std.setMaHS(rs.getInt("MaHS"));
                std.setHoTenHS(rs.getNString("TenHS"));
                std.setNgaySinh(rs.getDate("NgaySinh"));
                std.setGhiChu(rs.getNString("Ghichu"));
                std.setImage(rs.getBytes("ExtInfo"));

                vStudent.add(std);
            }
            stSelect.close();
        } catch (SQLException ex) {
            System.out.print(ex.toString());
        }
        return vStudent;
    }

    public void closeConnectSqlServer() {
        try {
            con.close();
        } catch (SQLException ex) {
            System.out.print(ex.toString());
        }
    }
}

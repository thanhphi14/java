/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Date;

/**
 *
 * @author NgoThanhPhi
 */
public class Student {

    private int maHS;
    private String hoTenHS;
    private Date ngaySinh;
    private String ghiChu;
    private byte[] image;

    public Student(String hoTenHS, Date ngaySinh, String ghiChu, byte[] image) {
        this.hoTenHS = hoTenHS;
        this.ngaySinh = ngaySinh;
        this.ghiChu = ghiChu;
        this.image = image;
    }

    public Student() {
    }

    public int getMaHS() {
        return maHS;
    }

    public void setMaHS(int maHS) {
        this.maHS = maHS;
    }

    public String getHoTenHS() {
        return hoTenHS;
    }

    public void setHoTenHS(String hoTenHS) {
        this.hoTenHS = hoTenHS;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NgoThanhPhi
 */
public class Utils {

    public Utils() {
    }

    // Trả về ngày với thời điểm bắt đầu    
    public Date dateNotTime(Date date) {
        // Get Calendar object set to the date and time of the given Date object
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    // Chuyển chuỗi ngày sang Ngày
    public Date parseDate(String date) {
        Date date2 = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            date2 = formatter.parse(date);
        } catch (Exception e) {
        }
        return date2;
    }

    public java.sql.Date parseSqlDate(java.util.Date utildate) {
        java.sql.Date sqlDate = null;
        try {
            sqlDate = new java.sql.Date(utildate.getTime());
        } catch (Exception ex) {
        }
        return sqlDate;
    }

    public java.util.Date parseUtilDate(java.sql.Date sqlDate) {
        java.util.Date utilDate = null;
        try {
            if (sqlDate != null) {
                utilDate = new Date(sqlDate.getTime());
            }
        } catch (Exception ex) {
        }
        return utilDate;
    }

    public byte[] convertFileToByte(String fileName) {
        FileInputStream fileInputStream = null;
        File file = new File(fileName);
        byte[] bFile = new byte[(int) file.length()];

        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (Exception ex) {
            bFile = null;
        }
        return bFile;
    }

    public JTable refreshTable(JTable rootTable, Vector<Student> vStudent) {
        JTable tbStudent = rootTable;
        DefaultTableModel model = (DefaultTableModel) tbStudent.getModel();
        model.setRowCount(0);

        int size = vStudent.size();
        for (int i = 0; i < size; i++) {
            Student std = vStudent.get(i);
            Object[] row = {std.getMaHS(), std.getHoTenHS(), std.getNgaySinh(), std.getGhiChu()};
            model.addRow(row);
        }

        return tbStudent;
    }
}
